﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSK_Algorytmy.Algorithm
{
    class MatrixDisplacementB
    {
        public string Encrypt(string passwordText, string keyText)
        {
            int[] key = new int[keyText.Length];
            int counter = 0;

            //Wyznaczenie kolejności alfabetycznej klucza
            for (char let = ' '; let <= '~'; ++let)
            {
                for (int i = 0; i < keyText.Length; ++i)
                {
                    if (keyText[i] == let)
                    {
                        key[i] = counter;
                        ++counter;
                    }
                }
            }
            //Wyliczenie liczby wierszy macierzy
            int rows = passwordText.Length / keyText.Length;
            rows = (passwordText.Length % keyText.Length) > 0 ? rows + 1 : rows;
            char[,] matrix = new char[rows, keyText.Length];

            //Wypełnienie macierzy pustymi polami
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < keyText.Length; ++j)
                    matrix[i, j] = ' ';
            }
            counter = 0;

            //Wypełnienie macierzy podanym hasłem
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < keyText.Length; ++j)
                {
                    if (counter < passwordText.Length)
                    {
                        if (passwordText[counter] == ' ')
                        {
                             matrix[i, j] = '_';
                            ++counter;
                        }
                          else
                        {
                        matrix[i, j] = passwordText[counter];
                        ++counter;
                         }
                    }
                }
            }

            //Część kodu odpowiedzialna za szyfrowanie oraz zczytywanie kolumn w kolejnosci alfabetycznej 
            string encryptedText = "";
            int number = 0;
            for (int i = 0; i < keyText.Length; ++i)
            {
                for (int k = 0; k < keyText.Length; ++k)
                {
                    if (key[k] == i)
                        number = k;
                }
                for (int j = 0; j < rows; ++j)
                {
                    if (matrix[j, number] != ' ')
                        encryptedText = encryptedText + matrix[j, number];
                }
                encryptedText += " ";
            }
            return encryptedText;
        }

        public string Decryption(string passwordText, string keyText)
        {
            int[] klucz = new int[keyText.Length];

            int counter = 0;
            //Wyznaczenie kolejności alfabetycznej klucza
            for (char letter = ' '; letter <= '~'; ++letter)
            {
                for (int i = 0; i < keyText.Length; ++i)
                {
                    if (keyText[i] == letter)
                    {
                        klucz[i] = counter;
                        ++counter;
                    }
                }
            }
            //Wyliczenie liczby wierszy macierzy
            int rows = (passwordText.Length % keyText.Length) > 0 ? (passwordText.Length / keyText.Length) + 1 : passwordText.Length / keyText.Length;
            char[,] matrix = new char[rows, keyText.Length];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < keyText.Length; ++j)
                    matrix[i, j] = ' ';
            }
            counter = 0;

            int letterindex = 0;

            //Wypełnienie macierzy podanym hasłem
            for (int i = 0; i < keyText.Length; ++i)
            {
                for (int j = 0; j < keyText.Length; ++j)
                {
                    if (klucz[j] == i)
                    {
                        for (int u = 0; u < rows; ++u)
                        {
                            if (passwordText[letterindex] != ' ')
                            {
                                matrix[u, j] = passwordText[letterindex];
                                ++letterindex;
                            }
                            else
                            {
                                ++letterindex;
                                break;
                            }
                        }
                    }
                }
            }
            //Odczytanie hasłą z macierzy
            string DecText = "";
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < keyText.Length; ++j)
                      if (matrix[i, j] == '_')
                       {
                      DecText += " ";
                      }
                       else
                      {
                    DecText += matrix[i, j];
                 }
            }
            return DecText;
        }
    }
}
