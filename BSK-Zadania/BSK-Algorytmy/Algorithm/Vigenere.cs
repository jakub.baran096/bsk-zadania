﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSK_Algorytmy.Algorithm
{
    class Vigenere
    {
        public string EncodeVigenere(string password, string key)
        {
            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";           
            string outText =  string.Empty;
         
            password = password.ToUpper();
            key = key.ToUpper();
            password.Replace(" ", "");
            for (int i = 0; i < password.Length; i++)
                outText += alphabet[(alphabet.IndexOf(password[i]) + alphabet.IndexOf(key[i % key.Length])) % alphabet.Length]; 
            
            return outText;
        }

        public string DecodeVigenere(string password, string key)
        {
            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
            string outText = string.Empty;
            password = password.ToUpper();
            key = key.ToUpper();

            for (int i = 0; i < password.Length; i++)
                outText += alphabet[(alphabet.IndexOf(password[i]) - alphabet.IndexOf(key[i % key.Length]) + alphabet.Length) % alphabet.Length]; 
            
            return outText;
        }
    }
}
