﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSK_Algorytmy.Algorithm
{
    public class DES
    {
        //Initial Permutation Table  
        private int[] InitialP = new int[] { 58,50,42,34,26,18,10,2,60,52,44,36,28,20,12,4,
                                       62,54,46,38,30,22,14,6,64,56,48,40,32,24,16,8,
                                       57,49,41,33,25,17,9,1,59,51,43,35,27,19,11,3,
                                       61,53,45,37,29,21,13,5,63,55,47,39,31,23,15,7 };

        //Circular Left shift Table (For Encryption)  
        private int[] ShiftTableE = new int[] { 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };

        //Circular Right shift Table (For Decryption)  
        private int[] ShiftTableD = new int[] { 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1, 1 };

        //Compression Permutation Table  / Permuted Choice-2
        private int[] PChoice2 = new int[] { 14,17,11,24,1,5,3,28,15,6,21,10,
                                                23,19,12,4,26,8,16,7,27,20,13,2,
                                                41,52,31,37,47,55,30,40,51,45,33,48,
                                                44,49,39,56,34,53,46,42,50,36,29,32 };

        //Expansion Permutation Table  
        private int[] ExpansionP = new int[] { 32,1,2,3,4,5,4,5,6,7,8,9,
                                               8,9,10,11,12,13,12,13,14,15,16,17,
                                               16,17,18,19,20,21,20,21,22,23,24,25,
                                               24,25,26,27,28,29,28,29,30,31,32,1 };

        //S Box Tables 
        public static int[,] SBox1 = new int[4, 16]{
                    {14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7},
                    {0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8},
                    {4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0},
                    {15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13}};
        public static int[,] SBox2 = new int[4, 16]{
                    {15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10},
                    {3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5},
                    {0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15},
                    {13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9}};
        public static int[,] SBox3 = new int[4, 16]{
                    {10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8},
                    {13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1},
                    {13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7},
                    {1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12}};
        public static int[,] SBox4 = new int[4, 16]{
                    {7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15},
                    {13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9},
                    {10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4},
                    {3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14}};
        public static int[,] SBox5 = new int[4, 16]{
                    {2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9},
                    {14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6},
                    {4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14},
                    {11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3}};
        public static int[,] SBox6 = new int[4, 16]{
                    {12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11},
                    {10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8},
                    {9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6},
                    {4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13}};
        public static int[,] SBox7 = new int[4, 16]{
                    {4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1},
                    {13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6},
                    {1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2},
                    {6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12}};
        public static int[,] SBox8 = new int[4, 16]{
                    {13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7},
                    {1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2},
                    {7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8},
                    {2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11}};
        public List<int[,]> SBoxesList = new List<int[,]> { SBox1, SBox2, SBox3, SBox4, SBox5, SBox6, SBox7, SBox8 };


        //Permutation after S-Box 
        private int[] P = new int[] { 16,7,20,21,29,12,28,17,1,15,23,26,5,18,31,10,
                                      2,8,24,14,32,27,3,9,19,13,30,6,22,11,4,25 };

        //Final Permutation Table  
        private int[] FinallyP = new int[] { 40,8,48,16,56,24,64,32,39,7,47,15,55,23,63,31,
                                             38,6,46,14,54,22,62,30,37,5,45,13,53,21,61,29,
                                             36,4,44,12,52,20,60,28,35,3,43,11,51,19,59,27,
                                             34,2,42,10,50,18,58,26,33,1,41,9,49,17,57,25 };

        int[] key = new int[56];
        int[] leftKey = new int[28];
        int[] rightKey = new int[28];
        int[] permutatedKey = new int[48];
        int[] text = new int[64];
        int[] leftText = new int[32];
        int[] rightText = new int[32];
        int[] tempRightText = new int[32];
        int[] tempLeftText = new int[32];
        int[] permutatedText = new int[48];
        int[] xoredTable = new int[48];
        int[] SBoxTable = new int[32];
        int[] loopResultTable = new int[32];
        int[] finalText = new int[64];
        int[] dataResult;

        private int[] InitialPermutation(int[] oldTable)
        {
            int tmp;
            int[] newTable = new int[64];
            for (int i = 0; i < 64; i++)
            {
                tmp = InitialP[i];
                newTable[i] = oldTable[tmp - 1];
            }
            return newTable;
        }

        private void Divide_LeftAndRight(int[] oldTable, int[] leftTable, int[] rightTable)
        {
            for (int i = 0, k = 0; i < oldTable.Length / 2; i++, ++k)
            {
                leftTable[k] = oldTable[i];
            }

            for (int i = oldTable.Length / 2, k = 0; i < oldTable.Length; i++, ++k)
            {
                rightTable[k] = oldTable[i];
            }
        }

        private void Connect_LeftAndRight(int[] newTable, int[] leftTable, int[] rightTable)
        {
            int j = 0;
            for (int i = 0; i < leftTable.Length; i++)
            {
                newTable[j++] = leftTable[i];
            }

            for (int i = 0; i < rightTable.Length; i++)
            {
                newTable[j++] = rightTable[i];
            }
        }

        private void Convert64BitsKeyTo52BitsKey(int[] key64, int[] key52)
        {
            for (int i = 0, j = 0; i < 64; i++)
            {
                if ((i + 1) % 8 == 0)
                    continue;
                key52[j++] = key64[i];
            }
        }

        private void SaveTemporaryTable(int[] table, int[] tempTable)
        {
            for (int i = 0; i < 32; i++)
            {
                tempTable[i] = table[i];
            }
        }

        private void CircularLeftShift(int[] keyToShift)
        {
            int FirstBit = keyToShift[0];
            for (int i = 0; i < keyToShift.Length - 1; i++)
            {
                keyToShift[i] = keyToShift[i + 1];
            }
            keyToShift[keyToShift.Length - 1] = FirstBit;
        }
        private void CircularRightShift(int[] keyToShift)
        {
            int i, LastBit = keyToShift[27];
            for (i = 27; i >= 1; --i)
            {
                keyToShift[i] = keyToShift[i - 1];
            }
            keyToShift[i] = LastBit;
        }

        private void PermutedChoice2(int[] oldTable, int[] newTable)
        {
            int temp;
            for (int i = 0; i < 48; i++)
            {
                temp = PChoice2[i];
                newTable[i] = oldTable[temp - 1];
            }
        }

        private void ExpansionPermutation(int[] oldTable, int[] newTable)
        {
            int temp;
            for (int i = 0; i < 48; i++)
            {
                temp = ExpansionP[i];
                newTable[i] = oldTable[temp - 1];
            }
        }

        private void XORTables(int[] inTable1, int[] inTable2, int[] outTable)
        {
            for (int i = 0; i < inTable1.Length; i++)
            {
                outTable[i] = inTable1[i] ^ inTable2[i];
            }
        }

        private int ConvertBitArrayToDecimal(int[] bitArray)
        {
            int decimalResult;
            string stringvalue = "";
            for (int i = 0; i < bitArray.Length; i++)
            {
                stringvalue += bitArray[i].ToString();
            }
            decimalResult = Convert.ToInt32(stringvalue, 2);
            return decimalResult;
        }

        public int[] ConvertDecimalToBitArray(int decimalnumber, int numberofbits)
        {
            int[] bitarray = new int[numberofbits];
            int k = numberofbits - 1;
            char[] bd = Convert.ToString(decimalnumber, 2).ToCharArray();

            for (int i = bd.Length - 1; i >= 0; --i, --k)
            {
                if (bd[i] == '1')
                    bitarray[k] = 1;
                else
                    bitarray[k] = 0;
            }

            while (k >= 0)
            {
                bitarray[k] = 0;
                --k;
            }

            return bitarray;
        }

        private void ConnectTableWithFragment(int[] mainTable, int[] tableFragment, int startIndex)
        {
            int j = startIndex;
            for (int i = 0; i < 4; i++)
            {
                mainTable[j++] = tableFragment[i];
            }
        }

        private void SBoxAlgorithm()
        {
            int[] row = new int[2];
            int[] column = new int[4];
            int rowindex, columnindex, sboxValue;
            int SBoxTableIndex;
            for (int SBoxNumber = 0, i = 0; i < 48; i += 6, SBoxNumber++)
            {
                row[0] = xoredTable[i];
                row[1] = xoredTable[i + 5];
                rowindex = ConvertBitArrayToDecimal(row);

                column[0] = xoredTable[i + 1];
                column[1] = xoredTable[i + 2];
                column[2] = xoredTable[i + 3];
                column[3] = xoredTable[i + 4];
                columnindex = ConvertBitArrayToDecimal(column);

                sboxValue = SBoxesList[SBoxNumber][rowindex, columnindex];

                int[] tempSBoxTableFragment = ConvertDecimalToBitArray(sboxValue, 4);

                SBoxTableIndex = SBoxNumber * 4;

                ConnectTableWithFragment(SBoxTable, tempSBoxTableFragment, SBoxTableIndex);
            }
        }

        private int[] Permutation(int[] oldTable)
        {
            int temp;
            int[] newTable = new int[32];
            for (int i = 0; i < 32; i++)
            {
                temp = P[i];
                newTable[i] = oldTable[temp - 1];
            }
            return newTable;
        }

        private void SwitchTables()
        {
            for (int i = 0; i < 32; i++)
            {
                leftText[i] = tempRightText[i];
                rightText[i] = loopResultTable[i];
            }
        }

        private void SixteenRounds()
        {
            for (int i = 0; i < 16; i++)
            {
                SaveTemporaryTable(rightText, tempRightText); //zapamiętanie prawej strony słowa

                Divide_LeftAndRight(key, leftKey, rightKey); //podzielenie klucza na lewą i prawą stronę

                //przesunięcie lewej i prawej strony klucza
                for (int j = 0; j < ShiftTableE[i]; j++)
                {
                    CircularLeftShift(leftKey);
                    CircularLeftShift(rightKey);
                }

                Connect_LeftAndRight(key, leftKey, rightKey); //połączenie lewej i prawej strony klucza w całość

                PermutedChoice2(key, permutatedKey); //permutacja klucza

                ExpansionPermutation(rightText, permutatedText); //permutacja prawej strony tekstu

                XORTables(permutatedText, permutatedKey, xoredTable); //xor prawego textu i klucza

                SBoxAlgorithm(); //S-Box

                SBoxTable = Permutation(SBoxTable); //permutacja

                XORTables(SBoxTable, leftText, loopResultTable);

                SwitchTables();
            }
        }

        private int[] FinalPermutation(int[] oldTable)
        {
            int temp;
            int[] newTable = new int[64];
            for (int i = 0; i < 64; i++)
            {
                temp = FinallyP[i];
                newTable[i] = oldTable[temp - 1];
            }
            return newTable;
        }

        private void DESEncryption()
        {
            text = InitialPermutation(text); //Initial Permutation

            Divide_LeftAndRight(text, leftText, rightText); //Podzielenie tekstu na lewą i prawą stronę 

            SixteenRounds(); //Pętla

            Connect_LeftAndRight(finalText, rightText, leftText); //Połączenie lewej  i prawej strony

            finalText = FinalPermutation(finalText); //Inverse Initial Permutation
        }

        public int[] Encrypt(int[] data, int[] key64)
        {
            Convert64BitsKeyTo52BitsKey(key64, key);
            int k, j;
            dataResult = new int[data.Length];
            //wydzielanie po 64 bity z danych, DESEncryption, laczenie 64 bitow w calosc
            for (int i = 0; i < data.Length; i += 64)
            {
                for (k = 0, j = i; j < (i + 64); ++j, ++k)
                {
                    text[k] = data[j];
                }
                DESEncryption();

                for (k = 0, j = i; j < (i + 64); ++j, ++k)
                {
                    dataResult[j] = finalText[k];
                }
            }
            return dataResult;
        }

        private void ReversedSixteenRounds()
        {
            for (int i = 0; i < 16; i++)
            {
                SaveTemporaryTable(rightText, tempRightText);
                PermutedChoice2(key, permutatedKey);
                ExpansionPermutation(rightText, permutatedText);
                XORTables(permutatedText, permutatedKey, xoredTable);
                SBoxAlgorithm();
                SBoxTable = Permutation(SBoxTable);
                XORTables(SBoxTable, leftText, loopResultTable);

                SwitchTables();

                Divide_LeftAndRight(key, leftKey, rightKey);

                for (int j = 0; j < ShiftTableD[i]; j++)
                {
                    CircularRightShift(leftKey);
                    CircularRightShift(rightKey);
                }

                Connect_LeftAndRight(key, leftKey, rightKey);
            }
        }

        private void StartDecryption()
        {
            text = InitialPermutation(text);

            Divide_LeftAndRight(text, leftText, rightText);

            ReversedSixteenRounds();

            Connect_LeftAndRight(finalText, rightText, leftText);

            finalText = FinalPermutation(finalText);
        }

        public int[] Decrypt(int[] data, int[] key64)
        {
            int j, k;

            Convert64BitsKeyTo52BitsKey(key64, key);
            dataResult = new int[data.Length];

            for (int i = 0; i < data.Length; i += 64)
            {
                for (k = 0, j = i; j < (i + 64); ++j, ++k)
                {
                    text[k] = data[j];
                }

                StartDecryption();

                for (k = 0, j = i; j < (i + 64); ++j, ++k)
                {
                    dataResult[j] = finalText[k];
                }
            }
            return dataResult;
        }

        public void Reverse(int[] array)
        {
            int length = array.Length;
            int mid = (length / 2);

            for (int i = 0; i < mid; i++)
            {
                int bit = array[i];
                array[i] = array[length - i - 1];
                array[length - i - 1] = bit;
            }
        }
    }
}
