﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSK_Algorytmy.Algorithm
{
    public class MatrixDisplacementA
    {
        public String MatrixEncryption(String code, String key)
        {
            int x, y, pom = 0, p = 0;
            char[] OutputTable, inputTable = code.ToCharArray();
            char[,] table;
            String string_object;
            List<int> numbers;

            numbers = new List<int>(Array.ConvertAll(key.Split('-'), int.Parse));
            x = 0;
            y = numbers.Count;

            if (code.Length % y != 0)
            {
                x = (code.Length / y) + 1;
            }
            else
            {
                x = code.Length / y;
            }

            table = new char[x, y];
            OutputTable = new char[x * y];

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (i == x - 1 && j == (code.Length % y) - 1)
                    {
                        table[i, j] = inputTable[pom];
                        break;
                    }
                    table[i, j] = inputTable[pom];
                    pom++;
                }
            }

            //szyfracja
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (table[i, numbers[j] - 1] != '\0')
                    {
                        OutputTable[p] = table[i, numbers[j] - 1];
                        p++;
                    }
                    else
                    {
                        OutputTable[p] = '*';
                        p++;
                    }
                }
            }

            string_object = new String(OutputTable);
            return string_object;
        }

        public String MatrixDescription(String code, String key)
        {
            int x, y, pomD = 0, q = 0;
            char[,] outDTable;
            char[] wynik, dTable = code.ToCharArray(); ;
            String string_objectD;
            List<int> numbers;

            numbers = new List<int>(Array.ConvertAll(key.Split('-'), int.Parse));
            x = 0;
            y = numbers.Count;

            if (code.Length % y != 0)
            {
                x = (code.Length / y) + 1;
            }
            else
            {
                x = code.Length / y;
            }

            outDTable = new char[x, y];
          
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    outDTable[i, numbers[j] - 1] = dTable[pomD];
                    pomD++;
                }
            }

            wynik = new char[x * y];
           
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    wynik[q] = outDTable[i, j];
                    q++;
                }
            }

            string_objectD = new String(wynik).Replace("*", String.Empty);

            return string_objectD;
        }
    }
}
