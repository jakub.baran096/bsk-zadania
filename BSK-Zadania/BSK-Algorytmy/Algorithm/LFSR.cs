﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSK_Algorytmy.Algorithm
{
    class LFSR
    {
        public byte[] SynchronousStreamCipher(byte[] file, string key)
        {
            byte[] Result = new byte[file.Length];
            byte[] keyByte = Encoding.ASCII.GetBytes(key);
            byte SingleByte = 0;
            int index = 0;        
            foreach (byte _byte in file)
            {
                SingleByte = (byte)(_byte ^ keyByte[index]);
                Result.SetValue(SingleByte, index);
                index++;
            }
            return Result;
        }
    }
}
