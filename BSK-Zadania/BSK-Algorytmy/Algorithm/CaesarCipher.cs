﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSK_Algorytmy.Algorithm
{
    class CaesarCipher
    {
        public String CaesarCipherEncryption(String Input, int Key)
        {
            char[] InputTable = Input.ToCharArray();
            String Output = "";
            int Character;
            Key = Key % 26;

            for (int i = 0; i < Input.Length; i++)
            {
                Character = (int)InputTable[i];

                if (Character >= 65 && Character <= 90)
                {
                    Character += Key;
                    if(Character < 65 || Character > 90) Character -= 26;
                }
                else if(Character >= 97 && Character <= 122)
                {
                    Character += Key;
                    if (Character < 97 || Character > 122) Character -= 26;
                }
                Output += (char)Character;
            }

            return Output;
        }

        public String CaesarCipherDescription(String Input, int Key)
        {
            char[] InputTable = Input.ToCharArray();
            String Output = "";
            int Character;
            Key = Key % 26;

            for (int i = 0; i < Input.Length; i++)
            {
                Character = (int)InputTable[i];

                if (Character >= 65 && Character <= 90)
                {
                    Character -= Key;
                    if (Character < 65 || Character > 90) Character += 26;
                }
                else if (Character >= 97 && Character <= 122)
                {
                    Character -= Key;
                    if (Character < 97 || Character > 122) Character += 26;
                }
                Output += (char)Character;
            }

            return Output;
        }
    }
}
