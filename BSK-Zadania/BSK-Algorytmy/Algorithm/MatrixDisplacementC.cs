﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSK_Algorytmy.Algorithm
{
    class MatrixDisplacementC
    {
        public String MatrixEncryptionC(String message, String keyText)
        {
            char[,] outETable;
            int suma, keyTmp = 0, counter=0, distance = 0, rows = 0, tmp = 0, number = 0;
            int[] klucz;
            char[] inputTable;
            string encryptedText = "";

            suma = message.Length;

            //key
            klucz = new int[keyText.Length];
            for (char let = ' '; let <= '~'; ++let)
            {
                for (int i = 0; i < keyText.Length; ++i)
                {

                    if (keyText[i] == let)
                    {
                        klucz[i] = counter;
                        ++counter;
                    }
                }
            }

            //rows
            while (suma > 0)
            {
                for (int i = 0; i < keyText.Length; i++)
                {
                    if (klucz[i] == keyTmp)
                    {
                        distance = i;
                        break;
                    }
                }
                suma -= distance + 1;
                rows++;
                keyTmp++;
            }

            //create matrix
            outETable = new char[rows, keyText.Length];
            inputTable = message.ToCharArray();
            suma = message.Length;
            keyTmp = 0;

            for (int i = 0; i < rows; i++)
            {
                if (suma > 0)
                {
                    for (int j = 0; j < keyText.Length; j++)
                    {
                        if (klucz[j] == keyTmp)
                        {
                            distance = j;
                            for (int t = 0; t <= distance; t++)
                            {
                                if (tmp < inputTable.Length)
                                {
                                    if (inputTable[tmp] == ' ')
                                    {
                                        outETable[i, t] = '*';
                                        tmp++;
                                    }
                                    else
                                    {
                                        outETable[i, t] = inputTable[tmp];
                                        tmp++;
                                    }
                                }
                            }
                            break;
                        }

                    }
                    suma -= distance + 1;
                    keyTmp++;
                }
            }

            //output string
            for (int i = 0; i < keyText.Length; ++i)
            {
                for (int k = 0; k < keyText.Length; ++k)
                {
                    if (klucz[k] == i)
                        number = k;
                }
                for (int j = 0; j < rows; ++j)
                {
                    if (outETable[j, number] != '\0')
                        encryptedText = encryptedText + outETable[j, number];
                }
                encryptedText += " ";
            }

            return encryptedText;
        }

        public String MatrixDescriptionC(String message, String keyText)
        {
            char[,] outDTable;
            char[] tab, wynik;
            String s, string_objectD;
            int suma, keyTmp = 0, counter = 0, distance = 0, rows = 0, a = 0, z = 0, o = 0;
            int[] klucz;
            string[] words;
            List<List<char>> charList;

            s = new String(message).Replace("*", String.Empty);
            suma = s.Length;
            words = message.Split(' ');
            charList = new List<List<char>>();

            //key
            klucz = new int[keyText.Length];
            for (char let = ' '; let <= '~'; ++let)
            {
                for (int i = 0; i < keyText.Length; ++i)
                {

                    if (keyText[i] == let)
                    {
                        klucz[i] = counter;
                        ++counter;
                    }
                }
            }

            //rows
            while (suma > 0)
            {
                for (int i = 0; i < keyText.Length; i++)
                {
                    if (klucz[i] == keyTmp)
                    {
                        distance = i;
                        break;
                    }
                }
                suma -= distance + 1;
                rows++;
                keyTmp++;
            }

            outDTable = new char[rows, keyText.Length];

            //string to char array
            for (int i = 0; i < keyText.Length; i++)
            {
                List<char> sublist = new List<char>();
                tab = words[a].ToCharArray();

                for (int q = 0; q < tab.Length; q++)
                {
                    sublist.Add(tab[q]);
                }
                charList.Add(sublist);
                a++;
            }

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < keyText.Length; j++)
                {
                    if (klucz[j] == i)
                    {
                        distance = j;
                    }
                }
                while (distance >= 0)
                {
                    if (charList[klucz[z]].Count == 0) { break; }
                    else
                    {
                        outDTable[i, z] = charList[klucz[z]][0];
                        charList[klucz[z]].RemoveAt(0);
                        z++;
                        distance--;
                    }
                }
                z = 0;
            }

            wynik = new char[message.Length];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < keyText.Length; j++)
                {
                    if (outDTable[i, j] != '\0')
                    {
                        wynik[o] = outDTable[i, j];
                        o++;
                    }
                }
            }

            string_objectD = new String(wynik).Replace("*", " ");
            return string_objectD;
        }
    }
}
