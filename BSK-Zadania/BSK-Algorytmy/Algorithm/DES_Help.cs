﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSK_Algorytmy.Algorithm
{
    class DES_Help
    {
        public static int NuberOfBits(int len)
        {
            int tmp = len / 8;
            int modulo = len % 8;
            if (modulo != 0)
                tmp += 1;
            return tmp = tmp * 64;

        }

        public static int[] ToBits(byte b)
        {
            int[] bitarray = new int[8];

            for (int i = 7; i >= 0; i--)
            {
                bitarray[i] = (b & (1 << i)) == 0 ? 0 : 1;
            }

            return bitarray;
        }

        public static void AssignArray1ToArray2b(int[] array1, int[] array2, int fromIndex)
        {
            int x, y;
            for (x = 0, y = fromIndex; x < array1.Length; x++, y++)
                array2[y] = array1[x];
        }

        public static byte[] IntTableToByteTable(int[] table)
        {

            int j = 0;
            int s = 0;
            int healp;
            int[] convertInt = new int[table.Length / 8];
            for (int i = 0; i < table.Length / 8; i++)
            {
                j = i * 8;
                s = 0;
                healp = j;
                for (int x = 0; x < 8; x++)
                {

                    s += Convert.ToInt32(Math.Pow(2, Convert.ToDouble(x)) * table[healp]);
                    healp += 1;
                }
                convertInt[i] = s;
            }
            byte[] b = convertInt.Select(i => (byte)i).ToArray();
            return b;
        }

        public static string RollKey()
        {
            string key = "";
            Random x = new Random();
            for (int i = 0; i < 8; i++)
            {
                char c = (char)x.Next(48, 122);
                key += c;
            }
            return key;
        }

    }
}
