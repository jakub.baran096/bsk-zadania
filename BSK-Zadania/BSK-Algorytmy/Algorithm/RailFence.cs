﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSK_Algorytmy.Algorithm
{
    class RailFence
    {
        public String RailFenceEncryption(string Input, int Key)
        {
            char[] InputTable = Input.ToCharArray();
            String[] OutputTable = new String[Key];

            int j = 0;
            bool Check = true;

            for (int i = 0; i < Input.Length; i++)
            {
                OutputTable[j] = OutputTable[j] + InputTable[i];

                if (Check) j++;
                else j--;

                if (Check == true && j == Key)
                {
                    Check = false;
                    j = Key - 2;
                }
                else if (Check == false && j == -1)
                {
                    Check = true;
                    j = 1;
                }
            }

            string Output = OutputTable[0];

            for (int i = 1; i < Key; i++) { Output += OutputTable[i]; }

            return Output;
        }

        public string RailFenceDescription(string Input, int Key)
        {
            char[] InputTable = Input.ToCharArray();
            char[] OutputTable = new char[Input.Length];

            bool Position;
            int Index = 0;
            int Number;

            for (int i = 0; i < Key; i++)
            {
                Number = i;
                Position = true;

                while (Number < Input.Length)
                {
                    OutputTable[Number] = InputTable[Index++];

                    if (i == 0 || i == Key - 1)
                    {
                        Number += 2 * (Key - 1);
                    }
                    else if (Position)
                    {
                        Position = false;
                        Number += 2 * (Key - i - 1);
                    }
                    else
                    {
                        Position = true;
                        Number += 2 * i;
                    }
                }
            }

            string Output = new string(OutputTable);
            return Output;
        }
    }



}
