﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSK_Algorytmy.Algorithm
{
    public class LinearFeedbackShiftRegisterGenerator
    {
        private List<int> List = new List<int>();
        private List<int> Input = new List<int>();

        public LinearFeedbackShiftRegisterGenerator(List<int> input, List<int> list)
        {
            Input = input;
            List = list;
        }

        public LinearFeedbackShiftRegisterGenerator(List<int> input)
        {
            Random random = new Random();
            Input = input;

            for (int i = 0; i < input[input.Count - 1]; i++) List.Add(random.Next(0, 2));
        }

        public int Generate()
        {
            int result = 0;

            foreach (var x in Input) result += List[x - 1];

            result %= 2;

            List.Insert(0, result);

            result = List[List.Count - 1];
            List.RemoveAt(List.Count - 1);

            return result;
        }
    }
}

