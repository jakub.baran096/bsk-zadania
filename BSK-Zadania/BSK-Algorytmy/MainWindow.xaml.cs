﻿using BSK_Algorytmy.Algorithm;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
namespace BSK_Algorytmy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        #region Drag and Drop - wprowadzanie 
        private void textBox2_PreviewDropPas1(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
            passwordTosend.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }

        private void textBox2_PreviewDropKey1(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
            keyTosend.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }

        private void textBox2_PreviewDropPas2(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
            passwordTosend2.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }

        private void textBox2_PreviewDropKey2(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
            keyTosend2.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }

        private void textBox2_PreviewDropPas3(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
            passwordTosend3.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }

        private void textBox2_PreviewDropKey3(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
            keyTosend3.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }

        private void textBox2_PreviewDropPas4(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
            passwordTosend4.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }

        private void textBox2_PreviewDropKey4(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
            keyTosend4.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }

        private void textBox2_PreviewDropPas5(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
            passwordTosend5.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }

        private void textBox2_PreviewDropKey5(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
            keyTosend5.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }

        private void textBox2_PreviewDropPas6(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
            passwordTosend6.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }

        private void textBox2_PreviewDropKey6(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
            keyTosend6.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }

        private void textBox2_PreviewDragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.All;
            e.Handled = true;
        }

        private void Window_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            TabItem t1 = Zakladki.SelectedItem as TabItem;
            MessageBox.Show(t1.Header.ToString());
        }


        private void passwordTosend_DragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.All;
            passwordTosend.Text = e.Data.GetData(DataFormats.Text).ToString();
        }

        #endregion


        #region Szyfrowanie / Odszyfrowanie wprowadzanie danych 
        private void Encrypt_Button_1(object sender, RoutedEventArgs e)
        {
            TabItem t1 = Zakladki.SelectedItem as TabItem;
            Stopwatch sw = new Stopwatch();
            if (t1.Header.ToString() == "Szyfr Płotkowy")
            {
                Regex regex = new Regex(@"^[0-9]*$");
                Match match = regex.Match(keyTosend.Text);
                if (passwordTosend.Text != "" && keyTosend.Text != "" && match.Success)
                {
                    RailFence rail = new RailFence();
                    string outText;
                    sw.Start();
                    outText = rail.RailFenceEncryption(passwordTosend.Text, Int32.Parse(keyTosend.Text));
                    sw.Stop();
                    Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                    MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                    Solution.Text = outText;
                }
                else
                {
                    MessageBox.Show("Uzupełnij poprawnie dane");
                }
            }
            else if (t1.Header.ToString() == "Przestawienie Macierzowe - A")
            {
                Regex regex = new Regex(@"^[0-9-]*$");
                Match match = regex.Match(keyTosend2.Text);

                if (passwordTosend2.Text != "" && keyTosend2.Text != "" && match.Success)
                {
                    MatrixDisplacementA rail = new MatrixDisplacementA();
                    String outText;
                    sw.Start();
                    outText = rail.MatrixEncryption(passwordTosend2.Text, keyTosend2.Text);
                    sw.Stop();
                    Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                    MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                    Solution2.Text = outText;
                }
                else
                {
                    MessageBox.Show("Uzupełnij poprawnie dane");
                }
            }
            else if (t1.Header.ToString() == "Przestawienie Macierzowe - B")
            {
                if (passwordTosend3.Text != "" && keyTosend3.Text != "")
                {
                    MatrixDisplacementB rail = new MatrixDisplacementB();
                    string outText;
                    sw.Start();
                    outText = rail.Encrypt(passwordTosend3.Text, keyTosend3.Text);
                    sw.Stop();
                    Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                    MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                    Solution3.Text = outText;
                }
                else
                {
                    MessageBox.Show("Uzupełnij poprawnie dane");
                }
            }
        }

        private void Encrypt_Button_2(object sender, RoutedEventArgs e)
        {
            TabItem t1 = Zakladki2.SelectedItem as TabItem;
            Stopwatch sw = new Stopwatch();
            if (t1.Header.ToString() == "Przestawienie Macierzowe - C")
            {
                Regex regexC = new Regex(@"^[A-Za-z]*$");
                Match matchC = regexC.Match(keyTosend4.Text);

                if (passwordTosend4.Text != "" && keyTosend4.Text != "" && matchC.Success)
                {
                    MatrixDisplacementC matrix = new MatrixDisplacementC();
                    string outText;
                    sw.Start();
                    outText = matrix.MatrixEncryptionC(passwordTosend4.Text, keyTosend4.Text);
                    sw.Stop();
                    Solution4.Text = outText;
                    Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                    MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                }
                else
                {
                    MessageBox.Show("Uzupełnij poprawnie dane");
                }
            }
            else if (t1.Header.ToString() == "Szyfr Cezara")
            {
                Regex regex = new Regex(@"^[0-9]*$");
                Match match = regex.Match(keyTosend5.Text);
                if (passwordTosend5.Text != "" && keyTosend5.Text != "" && match.Success)
                {

                    CaesarCipher caesarCipher = new CaesarCipher();
                    string outText;
                    sw.Start();
                    outText = caesarCipher.CaesarCipherEncryption(passwordTosend5.Text, Int32.Parse(keyTosend5.Text));
                    sw.Stop();
                    Solution5.Text = outText;
                    Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                    MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                }
                else
                {
                    MessageBox.Show("Uzupełnij poprawnie dane");
                }
            }
            else if (t1.Header.ToString() == "Szyfrowanie Vigenere'a")
            {
                Vigenere vig = new Vigenere();
                if (passwordTosend6.Text != "" && keyTosend6.Text != "")
                {
                    if (keyTosend6.Text.Any(char.IsDigit) != true && passwordTosend6.Text.Any(char.IsDigit) != true)
                    {
                        string outText;
                        sw.Start();
                        outText = vig.EncodeVigenere(passwordTosend6.Text, keyTosend6.Text).ToUpper();
                        sw.Stop();
                        Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                        MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                        Solution6.Text = outText;
                    }
                    else
                    {
                        MessageBox.Show("Uzupełnij poprawnie dane");
                    }
                }
                else
                {
                    MessageBox.Show("Uzupełnij poprawnie dane");
                }
            }
        }


        private void Encrypt_Button_3(object sender, RoutedEventArgs e)
        {
            TabItem t1 = Zakladki3.SelectedItem as TabItem;
            Stopwatch sw = new Stopwatch();
            if (t1.Header.ToString() == "Generator")
            {
                Regex regexC = new Regex(@"^[0-9 ,]*$");
                Match matchC = regexC.Match(passwordTosend7.Text);

                Regex regexC1 = new Regex(@"^[0-1 ,]*$");
                Match matchC1 = regexC1.Match(keyTosend7.Text);

                Solution7.Text = "";

                try
                {
                    if (passwordTosend7.Text != "" && matchC.Success)
                    {
                        string check1 = passwordTosend7.Text;
                        check1.Trim();
                        List<int> list1 = check1.Split(",").Select(Int32.Parse).ToList();
                        list1 = list1.Distinct().ToList();
                        list1.Sort();

                        if (keyTosend7.Text == "" && list1.Count > 0)
                        {
                            LinearFeedbackShiftRegisterGenerator linearFeedbackShiftRegisterGenerator = new LinearFeedbackShiftRegisterGenerator(list1);
                            sw.Start();
                            for (int i = 0; i < Math.Pow(2, list1[list1.Count - 1]) - 1; i++) Solution7.Text += linearFeedbackShiftRegisterGenerator.Generate().ToString() + " ";
                            sw.Stop();
                            Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                            MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");

                        }
                        else if (matchC1.Success && list1.Count > 0)
                        {
                            string check2 = keyTosend7.Text;
                            check2.Trim();
                            List<int> list2 = check2.Split(",").Select(Int32.Parse).ToList();

                            if (list2.Count == list1[list1.Count - 1])
                            {
                                LinearFeedbackShiftRegisterGenerator linearFeedbackShiftRegisterGenerator = new LinearFeedbackShiftRegisterGenerator(list1, list2);
                                sw.Start();

                                for (int i = 0; i < Math.Pow(2, list1[list1.Count - 1]) - 1; i++) Solution7.Text += linearFeedbackShiftRegisterGenerator.Generate().ToString() + " ";
                                sw.Stop();
                                Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                                MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                            }
                            else
                            {
                                MessageBox.Show("Uzupełnij poprawnie dane");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Uzupełnij poprawnie dane");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Uzupełnij poprawnie dane");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Uzupełnij poprawnie dane");
                }
            }
        }



        private void Decryption_Button_1(object sender, RoutedEventArgs e)
        {
            TabItem t1 = Zakladki.SelectedItem as TabItem;
            Stopwatch sw = new Stopwatch();
            if (t1.Header.ToString() == "Szyfr Płotkowy")
            {
                Regex regex = new Regex(@"^[0-9]*$");
                Match match = regex.Match(keyTosend.Text);

                if (passwordTosend.Text != "" && keyTosend.Text != "" && match.Success)
                {
                    RailFence rail = new RailFence();
                    string outText;
                    sw.Start();
                    outText = rail.RailFenceDescription(passwordTosend.Text, Int32.Parse(keyTosend.Text));
                    sw.Stop();
                    Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                    MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                    Solution.Text = outText;
                }
                else
                {
                    MessageBox.Show("Uzupełnij poprawnie dane");
                }
            }
            else if (t1.Header.ToString() == "Przestawienie Macierzowe - A")
            {
                Regex regex = new Regex(@"^[0-9-]*$");
                Match match = regex.Match(keyTosend2.Text);

                if (passwordTosend2.Text != "" && keyTosend2.Text != "" && match.Success)
                {
                    MatrixDisplacementA rail = new MatrixDisplacementA();
                    String outText;
                    sw.Start();
                    outText = rail.MatrixDescription(passwordTosend2.Text, keyTosend2.Text);
                    sw.Stop();
                    Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                    MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                    Solution2.Text = outText;
                }
                else
                {
                    MessageBox.Show("Uzupełnij poprawnie dane");
                }
            }
            else if (t1.Header.ToString() == "Przestawienie Macierzowe - B")
            {
                if (passwordTosend3.Text != "" && keyTosend3.Text != "")
                {
                    MatrixDisplacementB rail = new MatrixDisplacementB();
                    string outText;
                    sw.Start();
                    outText = rail.Decryption(passwordTosend3.Text, keyTosend3.Text);
                    sw.Stop();
                    Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                    MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                    Solution3.Text = outText;
                }
                else
                {
                    MessageBox.Show("Uzupełnij poprawnie dane");
                }
            }
        }


        private void Decryption_Button_2(object sender, RoutedEventArgs e)
        {
            TabItem t1 = Zakladki2.SelectedItem as TabItem;
            Stopwatch sw = new Stopwatch();

            if (t1.Header.ToString() == "Przestawienie Macierzowe - C")
            {
                Regex regexC = new Regex(@"^[A-Za-z]*$");
                Match matchC = regexC.Match(keyTosend4.Text);
                if (passwordTosend4.Text != "" && keyTosend4.Text != "" && matchC.Success)
                {

                    MatrixDisplacementC matrix = new MatrixDisplacementC();
                    string outText;
                    sw.Start();
                    outText = matrix.MatrixDescriptionC(passwordTosend4.Text, keyTosend4.Text);
                    sw.Stop();
                    Solution4.Text = outText;
                    Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                    MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                }
                else
                {
                    MessageBox.Show("Uzupełnij poprawnie dane");
                }
            }
            else if (t1.Header.ToString() == "Szyfr Cezara")
            {
                Regex regex = new Regex(@"^[0-9]*$");
                Match match = regex.Match(keyTosend5.Text);

                if (passwordTosend5.Text != "" && keyTosend5.Text != "" && match.Success)
                {
                    CaesarCipher caesarCipher = new CaesarCipher();
                    string outText;
                    sw.Start();
                    outText = caesarCipher.CaesarCipherDescription(passwordTosend5.Text, Int32.Parse(keyTosend5.Text));
                    sw.Stop();
                    Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                    MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                    Solution5.Text = outText;
                }
                else
                {
                    MessageBox.Show("Uzupełnij poprawnie dane");
                }
            }
            else if (t1.Header.ToString() == "Szyfrowanie Vigenere'a")
            {
                Vigenere vig = new Vigenere();

                if (passwordTosend6.Text != "" && keyTosend6.Text != "")
                {
                    sw.Start();
                    string outText;
                    outText = vig.DecodeVigenere(passwordTosend6.Text, keyTosend6.Text).ToUpper();
                    Solution6.Text = outText;
                    sw.Stop();
                    Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                    MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                }
                else
                {
                    MessageBox.Show("Uzupełnij poprawnie dane");
                }
            }
        }

        #endregion


        #region Odczyt plików 

        string filePath_Password;
        string filePath_Key;
        string file_Key_Value;
        private void Vine_Load_Password(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();

            dl.DefaultExt = ".txt";
            dl.Filter = "TXT|*.txt";
            dl.ShowDialog();
            filePath_Password = dl.FileName;


            string fileNameToShow = System.IO.Path.GetFileName(filePath_Password);
            MessageBox.Show("Pomyślnie wybrano plik: " + fileNameToShow);
            Vine_LoadDataFromFile_Password.Content = " " + fileNameToShow;
        }

        private void Vine_Load_Key(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();

            dl.DefaultExt = ".txt";
            dl.Filter = "TXT|*.txt";
            dl.ShowDialog();
            filePath_Key = dl.FileName;

            string fileNameToShow = System.IO.Path.GetFileName(filePath_Key);

            MessageBox.Show("Pomyślnie wybrano plik: " + fileNameToShow);
            Vine_LoadDataFromFile_Key.Content = fileNameToShow;

            using (StreamReader sr = File.OpenText(filePath_Key))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    file_Key_Value = s;
                }
            }
        }


        private void Matrix_b_Load_Password(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();

            dl.DefaultExt = ".txt";
            dl.Filter = "TXT|*.txt";
            dl.ShowDialog();
            filePath_Password = dl.FileName;


            string fileNameToShow = System.IO.Path.GetFileName(filePath_Password);
            MessageBox.Show("Pomyślnie wybrano plik: " + fileNameToShow);
            Matrix_b_LoadDataFromFile_Password.Content = " " + fileNameToShow;
        }

        private void Matrix_b_Load_Key(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();

            dl.DefaultExt = ".txt";
            dl.Filter = "TXT|*.txt";
            dl.ShowDialog();
            filePath_Key = dl.FileName;

            string fileNameToShow = System.IO.Path.GetFileName(filePath_Key);

            MessageBox.Show("Pomyślnie wybrano plik: " + fileNameToShow);
            Matrix_b_LoadDataFromFile_Key.Content = fileNameToShow;

            using (StreamReader sr = File.OpenText(filePath_Key))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    file_Key_Value = s;
                }
            }
        }

        private void Rail_Fence_Load_Password(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();

            dl.DefaultExt = ".txt";
            dl.Filter = "TXT|*.txt";
            dl.ShowDialog();
            filePath_Password = dl.FileName;


            string fileNameToShow = System.IO.Path.GetFileName(filePath_Password);
            MessageBox.Show("Pomyślnie wybrano plik: " + fileNameToShow);
            Rail_Fence_LoadDataFromFile_Password.Content = " " + fileNameToShow;
        }

        private void Rail_Fence_Load_Key(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();

            dl.DefaultExt = ".txt";
            dl.Filter = "TXT|*.txt";
            dl.ShowDialog();
            filePath_Key = dl.FileName;

            string fileNameToShow = System.IO.Path.GetFileName(filePath_Key);

            MessageBox.Show("Pomyślnie wybrano plik: " + fileNameToShow);
            Rail_Fence_LoadDataFromFile_Key.Content = fileNameToShow;

            using (StreamReader sr = File.OpenText(filePath_Key))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    file_Key_Value = s;
                }
            }
        }

        private void MEncryption_A_Load_Password(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();

            dl.DefaultExt = ".txt";
            dl.Filter = "TXT|*.txt";
            dl.ShowDialog();
            filePath_Password = dl.FileName;


            string fileNameToShow = System.IO.Path.GetFileName(filePath_Password);
            MessageBox.Show("Pomyślnie wybrano plik: " + fileNameToShow);
            MEncyption_A_LoadDataFromFile_Password.Content = " " + fileNameToShow;
        }

        private void MEncryption_A_Load_Key(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();

            dl.DefaultExt = ".txt";
            dl.Filter = "TXT|*.txt";
            dl.ShowDialog();
            filePath_Key = dl.FileName;

            string fileNameToShow = System.IO.Path.GetFileName(filePath_Key);

            MessageBox.Show("Pomyślnie wybrano plik: " + fileNameToShow);
            MEncryption_A_LoadDataFromFile_Key.Content = fileNameToShow;

            using (StreamReader sr = File.OpenText(filePath_Key))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    file_Key_Value = s;
                }
            }
        }

        private void MEncryption_C_Load_Password(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();

            dl.DefaultExt = ".txt";
            dl.Filter = "TXT|*.txt";
            dl.ShowDialog();
            filePath_Password = dl.FileName;


            string fileNameToShow = System.IO.Path.GetFileName(filePath_Password);
            MessageBox.Show("Pomyślnie wybrano plik: " + fileNameToShow);
            MEncyption_C_LoadDataFromFile_Password.Content = " " + fileNameToShow;
        }

        private void MEncryption_C_Load_Key(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();

            dl.DefaultExt = ".txt";
            dl.Filter = "TXT|*.txt";
            dl.ShowDialog();
            filePath_Key = dl.FileName;

            string fileNameToShow = System.IO.Path.GetFileName(filePath_Key);

            MessageBox.Show("Pomyślnie wybrano plik: " + fileNameToShow);
            MEncryption_C_LoadDataFromFile_Key.Content = fileNameToShow;

            using (StreamReader sr = File.OpenText(filePath_Key))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    file_Key_Value = s;
                }
            }
        }

        private void Caesar_Cipher_Load_Password(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();

            dl.DefaultExt = ".txt";
            dl.Filter = "TXT|*.txt";
            dl.ShowDialog();
            filePath_Password = dl.FileName;


            string fileNameToShow = System.IO.Path.GetFileName(filePath_Password);
            MessageBox.Show("Pomyślnie wybrano plik: " + fileNameToShow);
            Caesar_Cipher_LoadDataFromFile_Password.Content = " " + fileNameToShow;
        }

        private void Caesar_Cipher_Load_Key(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();

            dl.DefaultExt = ".txt";
            dl.Filter = "TXT|*.txt";
            dl.ShowDialog();
            filePath_Key = dl.FileName;

            string fileNameToShow = System.IO.Path.GetFileName(filePath_Key);

            MessageBox.Show("Pomyślnie wybrano plik: " + fileNameToShow);
            Caesar_Cipher_LoadDataFromFile_Key.Content = fileNameToShow;

            using (StreamReader sr = File.OpenText(filePath_Key))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    file_Key_Value = s;
                }
            }
        }
        #endregion

        // Kopiowanie do schowka
        #region Kopiowanie do schowka

        private void Copy_to_Clipboard(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Skopiowano!");
            TabItem t1 = Zakladki.SelectedItem as TabItem;
            TabItem t2 = Zakladki2.SelectedItem as TabItem;

            if (t1.Header.ToString() == "Szyfr Płotkowy")
            {
                Clipboard.SetText(Solution.Text);
            }
            else if (t1.Header.ToString() == "Przestawienie Macierzowe - A")
            {
                Clipboard.SetText(Solution2.Text);
            }
            else if (t1.Header.ToString() == "Przestawienie Macierzowe - B")
            {
                Clipboard.SetText(Solution3.Text);
            }

        }

        private void Copy_to_Clipboard2(object sender, RoutedEventArgs e)
        {
            TabItem t2 = Zakladki2.SelectedItem as TabItem;

            if (t2.Header.ToString() == "Przestawienie Macierzowe - C")
            {
                Clipboard.SetText(Solution4.Text);
            }
            else if (t2.Header.ToString() == "Szyfr Cezara")
            {
                Clipboard.SetText(Solution5.Text);
            }
            else if (t2.Header.ToString() == "Szyfrowanie Vigenere'a")
            {
                Clipboard.SetText(Solution6.Text);
            }

        }
        #endregion




        //Szyfrowanie / Odszyfrowanie z plików  
        #region Szyfrowanie / Odszyfrowanie z plików  
        private void Decryption_Button_2_file(object sender, RoutedEventArgs e)
        {
            Vigenere vig = new Vigenere();
            Stopwatch sw = new Stopwatch();
            int counter = 0;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter("Decode.txt", true))
            {
                using (StreamReader sr = File.OpenText(filePath_Password))
                {
                    string s = "";
                    sw.Start();
                    while ((s = sr.ReadLine()) != null)
                    {
                        //  MessageBox.Show(vig.EncodeVigenere(s, file_Key_Value).ToUpper());

                        file.WriteLine(vig.DecodeVigenere(s, file_Key_Value).ToUpper());
                        counter++;
                    }
                    sw.Stop();
                }
            }
            Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + "| Odkodowano: " + counter;
            MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "\n" + "Odkodowano: " + counter, "Podsumowanie");
        }

        private void Decryption_Button_1_1_file(object sender, RoutedEventArgs e)
        {
            try
            {
                RailFence railFence = new RailFence();
                Stopwatch sw = new Stopwatch();
                int counter = 0;
                using (System.IO.StreamWriter file = new System.IO.StreamWriter("Decode.txt", true))
                {
                    using (StreamReader sr = File.OpenText(filePath_Password))
                    {
                        string s = "";
                        sw.Start();
                        while ((s = sr.ReadLine()) != null)
                        {
                            file.WriteLine(railFence.RailFenceDescription(s, int.Parse(file_Key_Value)));
                            counter++;
                        }
                        sw.Stop();
                    }
                }
                Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + "| Odkodowano: " + counter;
                MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "\n" + "Odkodowano: " + counter, "Podsumowanie");
            }
            catch (Exception)
            {
                MessageBox.Show("Nieprawidłowe wczytano dane");
            }
        }


        private void Decryption_Button_1_2_file(object sender, RoutedEventArgs e)
        {
            MatrixDisplacementA matrix = new MatrixDisplacementA();
            Stopwatch sw = new Stopwatch();
            int counter = 0;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter("Decode.txt", true))
            {
                using (StreamReader sr = File.OpenText(filePath_Password))
                {
                    string s = "";
                    sw.Start();
                    while ((s = sr.ReadLine()) != null)
                    {
                        file.WriteLine(matrix.MatrixDescription(s, file_Key_Value));
                        counter++;
                    }
                    sw.Stop();
                }
            }
            Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + "| Odkodowano: " + counter;
            MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "\n" + "Odkodowano: " + counter, "Podsumowanie");
        }

        private void Decryption_Button_1_3_file(object sender, RoutedEventArgs e)
        {
            MatrixDisplacementB vig = new MatrixDisplacementB();
            Stopwatch sw = new Stopwatch();
            int counter = 0;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter("Decode.txt", true))
            {
                using (StreamReader sr = File.OpenText(filePath_Password))
                {
                    string s = "";
                    sw.Start();
                    while ((s = sr.ReadLine()) != null)
                    {
                        file.WriteLine(vig.Decryption(s, file_Key_Value));
                        counter++;
                    }
                    sw.Stop();
                }
            }
            Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + "| Odkodowano: " + counter;
            MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "\n" + "Odkodowano: " + counter, "Podsumowanie");
        }

        private void Decryption_Button_2_1_file(object sender, RoutedEventArgs e)
        {
            MatrixDisplacementC matrix = new MatrixDisplacementC();
            Stopwatch sw = new Stopwatch();
            int counter = 0;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter("Decode.txt", true))
            {
                using (StreamReader sr = File.OpenText(filePath_Password))
                {
                    string s = "";
                    sw.Start();
                    while ((s = sr.ReadLine()) != null)
                    {
                        file.WriteLine(matrix.MatrixDescriptionC(s, file_Key_Value));
                        counter++;
                    }
                    sw.Stop();
                }
            }
            Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + "| Odkodowano: " + counter;
            MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "\n" + "Odkodowano: " + counter, "Podsumowanie");
        }

        private void Decryption_Button_2_2_file(object sender, RoutedEventArgs e)
        {
            try
            {
                CaesarCipher caesarCipher = new CaesarCipher();
                Stopwatch sw = new Stopwatch();
                int counter = 0;
                using (System.IO.StreamWriter file = new System.IO.StreamWriter("Decode.txt", true))
                {
                    using (StreamReader sr = File.OpenText(filePath_Password))
                    {
                        string s = "";
                        sw.Start();
                        while ((s = sr.ReadLine()) != null)
                        {
                            file.WriteLine(caesarCipher.CaesarCipherDescription(s, int.Parse(file_Key_Value)));
                            counter++;
                        }
                        sw.Stop();
                    }
                }
                Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + "| Odkodowano: " + counter;
                MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "\n" + "Odkodowano: " + counter, "Podsumowanie");
            }
            catch (Exception)
            {
                MessageBox.Show("Nieprawidłowe wczytano dane");
            }
        }

        private void Encrypt_Button_1_1_file(object sender, RoutedEventArgs e)
        {
            try
            {
                RailFence railFence = new RailFence();
                Stopwatch sw = new Stopwatch();
                int counter = 0;
                using (System.IO.StreamWriter file = new System.IO.StreamWriter("Encode.txt", true))
                {
                    using (StreamReader sr = File.OpenText(filePath_Password))
                    {
                        string s = "";
                        sw.Start();
                        while ((s = sr.ReadLine()) != null)
                        {
                            file.WriteLine(railFence.RailFenceEncryption(s, int.Parse(file_Key_Value)));
                            counter++;
                        }
                        sw.Stop();
                    }
                }
                Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms| Zakodowano: " + counter;
                MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + " ms\n" + "Zakodowano: " + counter, "Podsumowanie");
            }
            catch (Exception)
            {
                MessageBox.Show("Nieprawidłowe wczytano dane");
            }
        }


        private void Encrypt_Button_1_2_file(object sender, RoutedEventArgs e)
        {
            MatrixDisplacementA matrix = new MatrixDisplacementA();
            Stopwatch sw = new Stopwatch();
            int counter = 0;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter("Encode.txt", true))
            {
                using (StreamReader sr = File.OpenText(filePath_Password))
                {
                    string s = "";
                    sw.Start();
                    while ((s = sr.ReadLine()) != null)
                    {
                        file.WriteLine(matrix.MatrixEncryption(s, file_Key_Value));
                        counter++;
                    }
                    sw.Stop();
                }
            }
            Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms| Zakodowano: " + counter;
            MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + " ms\n" + "Zakodowano: " + counter, "Podsumowanie");
        }

        private void Encrypt_Button_2_1_file(object sender, RoutedEventArgs e)
        {
            MatrixDisplacementC matrix = new MatrixDisplacementC();
            Stopwatch sw = new Stopwatch();
            int counter = 0;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter("Encode.txt", true))
            {
                using (StreamReader sr = File.OpenText(filePath_Password))
                {
                    string s = "";
                    sw.Start();
                    while ((s = sr.ReadLine()) != null)
                    {
                        file.WriteLine(matrix.MatrixEncryptionC(s, file_Key_Value));
                        counter++;
                    }
                    sw.Stop();
                }
            }
            Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms| Zakodowano: " + counter;
            MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + " ms\n" + "Zakodowano: " + counter, "Podsumowanie");
        }
        private void Encrypt_Button_2_2_file(object sender, RoutedEventArgs e)
        {
            try
            {
                CaesarCipher caesarCipher = new CaesarCipher();
                Stopwatch sw = new Stopwatch();
                int counter = 0;
                using (System.IO.StreamWriter file = new System.IO.StreamWriter("Encode.txt", true))
                {
                    using (StreamReader sr = File.OpenText(filePath_Password))
                    {
                        string s = "";
                        sw.Start();
                        while ((s = sr.ReadLine()) != null)
                        {
                            file.WriteLine(caesarCipher.CaesarCipherEncryption(s, int.Parse(file_Key_Value)));
                            counter++;
                        }
                        sw.Stop();
                    }
                }
                Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms| Zakodowano: " + counter;
                MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + " ms\n" + "Zakodowano: " + counter, "Podsumowanie");
            }
            catch (Exception)
            {
                MessageBox.Show("Nieprawidłowe wczytano dane");
            }
        }

        private void Encrypt_Button_2_file(object sender, RoutedEventArgs e)
        {
            Vigenere vig = new Vigenere();
            Stopwatch sw = new Stopwatch();
            int counter = 0;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter("Encode.txt", true))
            {
                using (StreamReader sr = File.OpenText(filePath_Password))
                {
                    string s = "";
                    sw.Start();
                    while ((s = sr.ReadLine()) != null)
                    {
                        file.WriteLine(vig.EncodeVigenere(s, file_Key_Value).ToUpper());
                        counter++;
                    }
                    sw.Stop();
                }
            }
            Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms| Zakodowano: " + counter;
            MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + " ms\n" + "Zakodowano: " + counter, "Podsumowanie");
        }

        private void Encrypt_Button_1_3_file(object sender, RoutedEventArgs e)
        {
            MatrixDisplacementB vig = new MatrixDisplacementB();
            Stopwatch sw = new Stopwatch();
            int counter = 0;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter("Ecrypt.txt", true))
            {
                using (StreamReader sr = File.OpenText(filePath_Password))
                {
                    string s = "";
                    sw.Start();
                    while ((s = sr.ReadLine()) != null)
                    {
                        //  MessageBox.Show(vig.EncodeVigenere(s, file_Key_Value).ToUpper());

                        file.WriteLine(vig.Encrypt(s, file_Key_Value));
                        counter++;
                    }
                    sw.Stop();
                }
            }
            Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + "| Odkodowano: " + counter;
            MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "\n" + "Odkodowano: " + counter, "Podsumowanie");
        }
        #endregion


        private string inputFilePath;

        private void Szyfr_Select_File(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Binary files (*.bin)|*.bin|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                inputFilePath = openFileDialog.FileName;
                LFSR_File.Content = "Zmien plik wejsciowy (" + openFileDialog.SafeFileName + ") na nowy";
                LFSR_File.Content = "Zmien plik wejsciowy (" + openFileDialog.SafeFileName + ") na nowy";
            }
        }

        private void LFSR_Szyfruj(object sender, RoutedEventArgs e)
        {
            if (inputFilePath==null)
            {
                MessageBox.Show("Najpierw wybierz plik!");
            }
            Regex regexC = new Regex(@"^[0-9 ,]*$");
            Match matchC = regexC.Match(Szyfr_wielomian.Text);
            Regex regexC1 = new Regex(@"^[0-1 ,]*$");
            Match matchC1 = regexC1.Match(Szyfr_Ziarno.Text);
            byte[] file = File.ReadAllBytes(inputFilePath);
            string resultGen = "";
            Stopwatch sw = new Stopwatch();
           
                if (Szyfr_wielomian.Text != "" && matchC.Success)
                {
                    string check1 = Szyfr_wielomian.Text;
                    check1.Trim();
                    List<int> list1 = check1.Split(",").Select(Int32.Parse).ToList();
                    list1 = list1.Distinct().ToList();
                    list1.Sort();
                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.Filter = "Binary files (*.bin)|*.bin|All files (*.*)|*.*";

                    string outputFilePath = "";
                    if (saveFileDialog.ShowDialog() == true)
                    {
                        outputFilePath = saveFileDialog.FileName;
                    }
                    if (Szyfr_Ziarno.Text == "" && list1.Count > 0)
                    {
                    byte[] file2 = File.ReadAllBytes(inputFilePath);
                    LinearFeedbackShiftRegisterGenerator linearFeedbackShiftRegisterGenerator = new LinearFeedbackShiftRegisterGenerator(list1);
                        sw.Start();
                        for (int i = 0; i < file2.Length; i++) resultGen += linearFeedbackShiftRegisterGenerator.Generate().ToString() + " ";
                        LFSR Szyfr = new LFSR();                    
                        BinaryWriter bw = new BinaryWriter(new FileStream(outputFilePath, FileMode.Create));                     
                        bw.Write(Szyfr.SynchronousStreamCipher(file2, resultGen));
                        bw.Close();
                        sw.Stop();
                        Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                        MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");

                    }
                    else if (matchC1.Success && list1.Count > 0)
                    {
                        string check2 = Szyfr_Ziarno.Text;
                        check2.Trim();
                        List<int> list2 = check2.Split(",").Select(Int32.Parse).ToList();

                        if (list2.Count == list1[list1.Count - 1])
                        {
                        byte[] file2 = File.ReadAllBytes(inputFilePath);
                        LinearFeedbackShiftRegisterGenerator linearFeedbackShiftRegisterGenerator = new LinearFeedbackShiftRegisterGenerator(list1, list2);
                            sw.Start();
                            for (int i = 0; i < file2.Length; i++) resultGen += linearFeedbackShiftRegisterGenerator.Generate().ToString() + " ";
                            LFSR Szyfr = new LFSR();
                            BinaryWriter bw = new BinaryWriter(new FileStream(outputFilePath, FileMode.Create));
                            bw.Write(Szyfr.SynchronousStreamCipher(file2, resultGen));
                            bw.Close();
                            sw.Stop();
                            Timer_text.Text = "Czas: " + sw.Elapsed.TotalMilliseconds + " ms";
                            MessageBox.Show("Czas działania: " + sw.Elapsed.TotalMilliseconds + "ms", "Podsumowanie");
                        }
                        else
                        {
                            MessageBox.Show("Uzupełnij poprawnie dane1");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Uzupełnij poprawnie dane2");
                    }
                }
                else
                {
                    MessageBox.Show("Uzupełnij poprawnie dane3");
                }               
        }



        #region zadanieDES

       private int[] key = new int[64];
       private string outputFilePath;

        private void InputFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Binary files (*.bin)|*.bin|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                inputFilePath = openFileDialog.FileName;
                File_Source_DES.Content = "Zmien plik wejsciowy (" + openFileDialog.SafeFileName + ") na inny";  File_Source_DES.Content = "Zmien plik wejsciowy (" + openFileDialog.SafeFileName + ") na inny";
            }
        }

        private void Start_DES(object sender, RoutedEventArgs e)
        {

            byte[] file = File.ReadAllBytes(inputFilePath);
            int[] buffor = new int[DES_Help.NuberOfBits(file.Length)];
            int[] bitsTable = new int[1];
            int j = 0;
            for (int i = 0; i < file.Length; i++)
            {
                bitsTable = DES_Help.ToBits(file[i]);
                j = i * 8;
                DES_Help.AssignArray1ToArray2b(bitsTable, buffor, j);
            }


            if (des_key.Text.Length > 8)
                MessageBox.Show("Klucz musi mieć 64 bity");
            else
            {
                string keyString = des_key.Text;
                byte[] keyByte = Encoding.ASCII.GetBytes(keyString);

                for (int i = 0; i < keyByte.Length; i++)
                {
                    bitsTable = DES_Help.ToBits(keyByte[i]);
                    j = i * 8;
                    DES_Help.AssignArray1ToArray2b(bitsTable, key, j);
                }
            }
            DES des = new DES();
            int[] result = des.Encrypt(buffor, key);
            byte[] byteResult = DES_Help.IntTableToByteTable(result);

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Binary files (*.bin)|*.bin|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                outputFilePath = saveFileDialog.FileName;
            }
            BinaryWriter bw = new BinaryWriter(new FileStream(outputFilePath, FileMode.Create));
            bw.Write(byteResult);
            bw.Close();

        }

        private void DES_Decryption(object sender, RoutedEventArgs e)
        {

            byte[] file = File.ReadAllBytes(inputFilePath);
            int[] buffor = new int[DES_Help.NuberOfBits(file.Length)];
            int[] bitsTable = new int[1];
            int j = 0;
            for (int i = 0; i < file.Length; i++)
            {
                bitsTable = DES_Help.ToBits(file[i]);
                j = i * 8;
                DES_Help.AssignArray1ToArray2b(bitsTable, buffor, j);
            }


            if (des_key.Text.Length > 8)
                MessageBox.Show("Klucz musi mieć 64 bity");
            else
            {
                string keyString = des_key.Text;
                byte[] keyByte = Encoding.ASCII.GetBytes(keyString);

                for (int i = 0; i < keyByte.Length; i++)
                {
                    bitsTable = DES_Help.ToBits(keyByte[i]);
                    j = i * 8;
                    DES_Help.AssignArray1ToArray2b(bitsTable, key, j);
                }
            }
            DES des = new DES();
            int[] result = des.Decrypt(buffor, key);
            byte[] byteResult = DES_Help.IntTableToByteTable(result);

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Binary files (*.bin)|*.bin|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                outputFilePath = saveFileDialog.FileName;
            }
            BinaryWriter bw = new BinaryWriter(new FileStream(outputFilePath, FileMode.Create));
            bw.Write(byteResult);
            bw.Close();
        }

        #endregion








    }



}
